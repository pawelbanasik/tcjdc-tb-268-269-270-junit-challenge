package com.pawelbanasik;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UtilitiesTest {

	private Utilities utilities;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		utilities = new Utilities();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void everyNthChar() {
		char[] sourceArray = { 'h', 'e', 'l', 'l', 'o' };
		int n = 2;
		char[] expected = { 'e', 'l' };

		assertArrayEquals(expected, utilities.everyNthChar(sourceArray, n));

	}

	@Test
	public void everyNthChar_nGreaterThanArrayLength() {
		char[] sourceArray = { 'h', 'e', 'l', 'l', 'o' };
		int n = 8;
		char[] expected = { 'h', 'e', 'l', 'l', 'o' };

		assertArrayEquals(expected, utilities.everyNthChar(sourceArray, n));

	}

	@Test
	public void removePairs() {
		String inputOne = utilities.removePairs("AABBCDDEFF");
		String outputOne = "ABCDEF";
		String inputTwo = utilities.removePairs("ABCCABDEEF");
		String outputTwo = "ABCABDEF";

		assertEquals(inputOne, outputOne);
		assertEquals(inputTwo, outputTwo);
		assertNull("Did not get null returned when argument passed was null", utilities.removePairs(null));
		assertEquals("A", utilities.removePairs("A"));
		assertEquals("", "");

	}

	@Test
	public void converter() {
		int a = 10;
		int b = 5;
		int expected = 300;
		assertEquals(expected, utilities.converter(a, b));

	}

	@Test (expected = ArithmeticException.class)
 	public void converter_arithmeticExceptionCheck() {
		int a = 10;
		int b = 0;
		
		utilities.converter(a, b);
		
		
	}
	
	@Test
	public void nullIfOddLength() {
		String sourceOne = "Michal";
		String sourceTwo = "Pawel";

		assertNotNull(utilities.nullIfOddLength(sourceOne));
		assertNull(utilities.nullIfOddLength(sourceTwo));

	}

}
