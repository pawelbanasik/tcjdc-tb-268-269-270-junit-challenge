package com.pawelbanasik;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class UtilitiesTestParameterized {

	private Utilities utilities;
	private String source;
	private String expected;

	
	
	public UtilitiesTestParameterized(String source, String expected) {
		this.source = source;
		this.expected = expected;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		utilities = new Utilities();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Parameterized.Parameters
	public static Collection<Object[]> testConditions(){
		return Arrays.asList(new Object[][]{
			{"ABCDEFF", "ABCDEF"},
			{"AB88EFFG", "AB8EFG"},
			{"112233445566", "123456"},
			{"ZYZQQB", "ZYZQB"},
			{"A", "A"}
		});
	}
	
	
	@Test
	public void testRemovePairs() {
		utilities.removePairs(source);
		assertEquals(expected, utilities.getWord());
		
	}

}
